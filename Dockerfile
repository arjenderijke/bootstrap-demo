## -*- docker-image-name: "bootstrap-demo" -*-
# https://nodejs.org/en/docs/guides/nodejs-docker-webapp/
FROM debian:stable

RUN apt-get update && apt-get install -y \
    nodejs \
    nodejs-legacy \
    npm

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.json /usr/src/app/
RUN npm install

# Bundle app source
COPY . /usr/src/app

EXPOSE 3000

CMD [ "npm", "start" ]
