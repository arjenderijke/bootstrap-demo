const express = require('express');
var serveStatic = require('serve-static')

var app = express();
const port = process.env.PORT || 3000;

app.use(serveStatic('examples', {'index': ['index.html']}))

app.listen(port);
console.log('Running on http://localhost:' + port);
